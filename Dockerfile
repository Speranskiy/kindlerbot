FROM python:3-alpine

WORKDIR /app

RUN mkdir books
RUN pip install --no-cache-dir python-telegram-bot==13.15 sqlalchemy==1.4.48 unidecode
RUN chmod 1777 /tmp

COPY helpers.py .
COPY bot.py .
RUN mkdir /data

CMD [ "python", "/app/bot.py" ]

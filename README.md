# Kindlerbot
## Overview
This bot is able to send *.mobi and *.epub files to configured email @kindle.com address. Usually it's being used to send books to Kindle readers.

## How to use
1. Send /email and follow instructions
2. Send a file to the bot to get it delivered to your Kindle reader.

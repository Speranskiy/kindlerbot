#!/usr/bin/python3
from sqlalchemy import create_engine, MetaData, Table, Column, Text, UniqueConstraint, Boolean
from sqlalchemy.dialects.sqlite import insert
import telegram, time, os
from telegram.ext import Updater
from telegram.error import (TelegramError, Unauthorized, BadRequest,
                            TimedOut, ChatMigrated, NetworkError)
TOKEN = os.environ["TOKEN"]

updater = Updater(token=TOKEN)
db_path = os.environ["DB_PATH"]
db_connect = create_engine(
        f'sqlite:///{db_path}/users.db', 
        connect_args={'check_same_thread': False})
conn = db_connect.connect()

text = "Яндекс заблокировал учётную запись, которую мы использовали для отправки книг (kindlerbot@yandex.ru). Новый адрес почты бота: kindlerbot@gmx.com. Вам необходимо добавить его в доверенные адреса в настройках учётной записи Amazon."

query = f'SELECT bookAddress FROM USERS WHERE userId = {update.message.chat_id}'
users = conn.execute(query).fetchall()
for u in users:
    try:
        updater.bot.send_message(chat_id=u["userId"], text=text, parse_mode="HTML")
        print("Sent to id: "+str(u["userId"]))
    except Unauthorized:
        print("Blocked by: "+str(u["userId"]))
    time.sleep(5)


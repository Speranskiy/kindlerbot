#!/usr/bin/python
from telethon.sync import TelegramClient, errors
import os
from time import sleep

tth_id = os.environ.get("TTH_ID")
tth_hash = os.environ.get("TTH_HASH")
botname = os.environ.get("BOT_NAME")
tests=[
    ("/help", "@kindle.com"),
    ("/check", "Aдрес"),
    ("/email", "Aдрес")
    ]

def connect():
    client = TelegramClient("bot-test/.anon.session", tth_id, tth_hash).start()
    return client

def test_reply(client, data):
    client.send_message(botname, data[0])
    sleep(3)
    msgs = client.get_messages(botname, limit=1)
    client.send_read_acknowledge(botname, msgs[0])
    if data[1] in msgs[0].message:
        print("Test {} OK".format(data[0]))
    else:
        print("{} not found in {}".format(data[1],msgs[0].message))
        raise IOError("Test {} failed!".format(data[0]))


def main():
    client = connect()
    for test in tests:
        test_reply(client, test)


if __name__ == '__main__':
    main()

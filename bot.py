#!/usr/bin/python3
from telegram.ext import Updater,\
                         CommandHandler,\
                         MessageHandler,\
                         Filters,\
                         ConversationHandler
import logging
from telegram.error import (TelegramError, Unauthorized, BadRequest,
                            TimedOut, ChatMigrated, NetworkError)
from telegram.constants import MAX_MESSAGE_LENGTH

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders

from sqlalchemy import create_engine, MetaData, Table, Column, Text, UniqueConstraint, Boolean
from sqlalchemy.dialects.sqlite import insert

import os
import re
import unidecode

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

db_path = os.environ["DB_PATH"]
db_connect = create_engine(
        f'sqlite:///{db_path}/users.db', 
        connect_args={'check_same_thread': False})
# sqlalchemy binding MetaData container
USERS = MetaData(bind=db_connect)

USERS_TABLE = Table("users", USERS,
                     Column("userId", Text),
                     Column("bookAddress", Text),
                     UniqueConstraint('userId', name='uix_1')
                     )
USERS.create_all(db_connect, checkfirst=True)
conn = db_connect.connect()

EMAIL, SENDER = range(2)

def start (update, context):
    context.bot.send_message(chat_id=update.message.chat_id, text="Бот отправляет файлы с форматом mobi, epub и pdf на адрес электронной почты в домене @kindle.com и @pbsync.com.\n Отправьте /email для установки электронного адреса @kindle.com.\n Отправка происходит с адреса kindlerbot@gmx.com, добавьте этот ящик в список доверенных адресов в настройках аккаунта Amazon.\n Если вам не приходит отправленная книга, значит срабатывает защита Amazon. Для того, чтобы книги начали отправляться, измените адрес книги в настройках учётной записи Amazon на любой другой и поменяйте настройки в боте.\n Исходный код бота тут -> https://gitlab.com/Speranskiy/kindlerbot")

def help (update, context):
    context.bot.send_message(chat_id=update.message.chat_id, text="Бот отправляет файлы с форматом mobi, epub и pdf на адрес электронной почты в домене @kindle.com и @pbsync.com.\n Отправьте /email для установки электронного адреса @kindle.com.\n Отправка происходит с адреса kindlerbot@gmx.com, добавьте этот ящик в список доверенных адресов в настройках аккаунта Amazon.\n Если вам не приходит отправленная книга, значит срабатывает защита Amazon. Для того, чтобы книги начали отправляться, измените адрес книги в настройках учётной записи Amazon на любой другой и поменяйте настройки в боте.\n Исходный код бота тут -> https://gitlab.com/Speranskiy/kindlerbot")

def email(update, context):
    rowsQuery = f'SELECT Count() FROM USERS where userId = {update.message.chat_id}'
    if not conn.execute(rowsQuery).fetchone()[0]:
        update.message.reply_text('Окей, отправьте мне адрес электронной почты, на который хотите отправлять книги. Принимается только адрес, оканчивающийся на kindle.com и pbsync.com (экспериментально)')
    else:
        update.message.reply_text('А у вас уже есть запись в базе, вот что там сохранено:')
        check(update, context)
        return ConversationHandler.END
    return EMAIL

def success(update, context):
    regex_mail = re.compile(r"[^@]+@[^@]+\.[^@]+")
    if regex_mail.match(update.message.text):
        if update.message.text.lower().endswith('@kindle.com') or update.message.text.lower().endswith('@pbsync.com'):
            context.user_data['kindle_mail']=update.message.text
            insert_stmt = insert(USERS_TABLE).values(
                    userId = update.message.chat_id,
                    bookAddress = context.user_data["kindle_mail"])
            conn.execute(insert_stmt)
            update.message.reply_text('Сохранил! Теперь можете пересылать мне файлы mobi, epub или fb2, я отправлю их на указанный адрес.')
        else:
            update.message.reply_text('Нет, так дело не пойдет. Подойдет только почта, оканчивающаяся на @kindle.com, а вы прислали '+update.message.text+' Попробуйте начать снова.')
            return ConversationHandler.END
    else:
        update.message.reply_text('Вы, видимо, ошиблись с вводом почтового адреса. Попробуйте начать сначала.')
    return ConversationHandler.END

def cancel(update, context):
    update.message.reply_text("Отменено")
    return ConversationHandler.END

def book(update, context):
    if update.message.document.file_name.lower().endswith(('.mobi', '.epub', '.pdf')):
        try:
            query = f'SELECT bookAddress FROM USERS WHERE userId = {update.message.chat_id}'
            bookAddress = conn.execute(query).fetchone()[0]
        except:
            update.message.reply_text('Вы не настроили почтовый адрес вашей книги. Отправьте /email для добавления. Или /check, чтобы посмотреть настройки.')
            return
        try:
            doc_file = context.bot.get_file(update.message.document.file_id)
        except BadRequest as e:
            if str(e) == "File is too big":
                context.bot.send_message(chat_id=update.message.chat_id, text="Файл слишком большой. В связи с ограничениями в Telegram, я не могу обработать файл, больше 20МБайт. Может быть, почитаете что-нибудь другое?")
                return
        file_book = "/app/books/"+update.message.document.file_name
        doc_file.download(file_book)

        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(file_book, "rb").read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="%s"' % unidecode.unidecode(os.path.basename(file_book)))
        msg = MIMEMultipart()
        msg['From'] = os.environ.get("SMTP_USER")
        msg['To'] = bookAddress
        msg.attach(part)

        try:
            smtp = smtplib.SMTP_SSL(os.environ.get("SMTP_SERVER"), os.environ.get("SMTP_PORT"))
            smtp.ehlo()
            smtp.login(os.environ.get("SMTP_USER"), os.environ.get("SMTP_PASS"))
            smtp.sendmail(os.environ.get("SMTP_USER"), bookAddress, msg.as_string())
            update.message.reply_text('Успешно отправлено на '+bookAddress+'.')
        except smtplib.SMTPException as e:
            print("Error: unable to send email: "+str(e))
            update.message.reply_text('Oшибка отправки на '+bookAddress+'.')
        finally:
            smtp.quit()
            os.remove(file_book)

    else:
        update.message.reply_text('Я могу отправлять только файлы *.mobi, *.epub и *.fb2, а вы пытаетесь отправить '+update.message.document.file_name) 
    
def check(update, context):
    try:
        query = f'SELECT bookAddress FROM USERS WHERE userId = {update.message.chat_id}'
        bookAddress = conn.execute(query).fetchone()[0]
        update.message.reply_text('Aдрес вашей электронной книги: '+bookAddress)
    except:
        update.message.reply_text('Вы не настроили почтовый адрес вашей книrи. Отправьте /email для добавления.')

def remove(update, context):
    rowsQuery = f'SELECT Count() FROM USERS where userId = {update.message.chat_id}'
    if conn.execute(rowsQuery).fetchone()[0]:
        query = f'DELETE FROM USERS WHERE userId = {update.message.chat_id}'
        conn.execute(query)
        update.message.reply_text('Ваша запись успешно удалена.')
    else:
        update.message.reply_text('Вы не настроили почтовый адрес вашей книrи. Отправьте /email для добавления.')

def error_callback(update, context):
    try:
        raise error
    except Unauthorized:
        print("Unauthorized error")
        # remove update.message.chat_id from conversation list
    except BadRequest as e:
        # handle malformed requests - read more below!
        print("BadRequest error: "+str(e))
        print(MAX_MESSAGE_LENGTH)
    except TimedOut:
        # handle slow connection problems
        print("TimedOut error")
    except NetworkError:
        # handle other connection problems
        print("Network error")
    except ChatMigrated as e:
        # the chat_id of a group has changed, use e.new_chat_id instead
        print("ChatMigrated error: "+str(e))
    except TelegramError:
        # handle all other telegram related errors
        print("TelegramError error")


def main():
    updater = Updater(token=os.environ.get("TOKEN"), use_context=True)
    dispatcher = updater.dispatcher
    
    conv_handler = ConversationHandler(
            entry_points=[CommandHandler('email', email)],
            
            states={
                EMAIL: [MessageHandler(Filters.text, success, pass_user_data=True)],
                },
            fallbacks=[CommandHandler('cancel', cancel)]
                )
    dispatcher.add_handler(conv_handler)
    book_handler = MessageHandler(Filters.document, book)
    dispatcher.add_handler(book_handler)
    start_handler = CommandHandler('start', start)
    dispatcher.add_handler(start_handler)
    check_handler = CommandHandler('check', check)
    dispatcher.add_handler(check_handler)
    remove_handler = CommandHandler('remove', remove)
    dispatcher.add_handler(remove_handler)
    help_handler = CommandHandler('help', help)
    dispatcher.add_handler(help_handler)
    dispatcher.add_error_handler(error_callback)
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
